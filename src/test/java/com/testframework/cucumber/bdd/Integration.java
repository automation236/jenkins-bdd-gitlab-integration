package com.testframework.cucumber.bdd;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Integration {
    @Given("^I want to setup CI CD$")
    public void i_want_to_setup_CI_CD() throws Throwable {
        System.out.println("Given clause of jenkins integration");
    }

    @When("^I trigger the build from Jenkins$")
    public void i_trigger_the_build_from_Jenkins() throws Throwable {
        System.out.println("When clause of jenkins integration");
    }

    @Then("^I am displayed with the Jenkins build status$")
    public void i_am_displayed_with_the_Jenkins_build_status() throws Throwable {
        System.out.println("Then clause of jenkins integration");
    }

}
