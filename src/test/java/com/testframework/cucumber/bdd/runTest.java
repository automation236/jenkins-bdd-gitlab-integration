package com.testframework.cucumber.bdd;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        //features = "classpath:registration.feature" ,
        features = "src/test/resources/integration.feature",
        glue = "classpath:com.testframework.cucumber.bdd"
)

public class runTest {
}
